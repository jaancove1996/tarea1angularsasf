import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DirectivasComponent } from './directivas/directivas.component';
import { NuevomoduloModule } from './nuevomodulo/nuevomodulo.module';
//import { PeliculasComponent } from './nuevomodulo/peliculas/peliculas.component';

@NgModule({
  declarations: [
    AppComponent,
    DirectivasComponent
  ],
  imports: [
    BrowserModule,
    NuevomoduloModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
